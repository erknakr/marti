import React from "react";
import logo from '../assets/img/Logo-Blue.png'

const HeaderWhite = () => {
    return (
            <header className="header-white">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <img src={logo} className="App-logo" alt="logo" />
                        </div>
                    </div>
                </div>
            </header>
    );
};

export default HeaderWhite;