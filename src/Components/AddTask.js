import React, { Component } from "react";
import firebase from "firebase";

class AddTask extends Component {
  constructor(props) {
    super(props);
    this.state = { value: "" };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();

    const user = firebase.auth().currentUser;
    const email = user.uid;

    const dates = new Date().getTime();

    firebase
      .database()
      .ref("to-do/" + email + "/" + dates)
      .set({
        task: this.state.value,
        date: firebase.database.ServerValue.TIMESTAMP,
        userauth: email,
        active: "true",
      });
    this.setState({
      value: "",
    });
  }

  twoEvent = (e) => {
    this.handleSubmit(e);
    this.props.refresh();
  }

  render() {
    return (
      <div className="add-task">
        <form onSubmit={this.twoEvent}>
          <input
            type="text"
            name="taskData"
            id="taskData"
            placeholder="Add Task"
            value={this.state.value}
            onChange={this.handleChange}
          />
          <button type="submit" hidden></button>
        </form>
      </div>
    );
  }
}

export default AddTask;
