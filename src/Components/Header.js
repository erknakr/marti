import React from "react";
import logo from '../assets/img/Logo.png'

const Header = () => {
    return (
            <header>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <img src={logo} className="App-logo" alt="logo" />
                        </div>
                    </div>
                </div>
            </header>
    );
};

export default Header;