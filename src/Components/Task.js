import React, { Component } from "react";
import firebase from 'firebase';

class Task extends Component {

  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});

    const status = event.target.attributes.getNamedItem('activite').value;
    const id = event.target.attributes.getNamedItem('id').value;

    const user = firebase.auth().currentUser;
    const email = user.uid;

    if (status === 'false') {
      event.target.attributes.getNamedItem('activite').value = "true";
      return firebase.database().ref("to-do/" + email + '/' + id).update({
        active: "true"
      });
    } else {
      event.target.attributes.getNamedItem('activite').value = "false";
      return firebase.database().ref("to-do/" + email + '/' + id).update({
        active: "false"
      });
    }

  }

  render() {
    return (
      <div className={"tasks " + this.props.active}>
        <form>
          <input
            type="checkbox"
            name="taskData"
            id={this.props.date}
            placeholder="Add Task"
            value={this.props.date}
            onChange={this.handleChange}
            activite={this.props.active}
          />
          <label for={this.props.date}>{this.props.task}</label>
        </form>
      </div>
    );
  }
}

export default Task;
