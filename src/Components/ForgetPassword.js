import React, { useCallback, useContext } from "react";
import { withRouter, Redirect } from "react-router";
import firebaseConfig from "../firebase.js";
import { AuthContext } from "../Auth.js";
import HeaderWhite from "./HeaderWhite";

const ForgetPassword = ({ history }) => {
  const handleForget = useCallback(
    async (event) => {
      event.preventDefault();
      const { email } = event.target.elements;
      try {
        await firebaseConfig
          .auth()
          .sendPasswordResetEmail (email.value);
        history.push("/Login");
      } catch (error) {
        alert(error);
      }
    },
    [history]
  );

  const { currentUser } = useContext(AuthContext);

  if (currentUser) {
    return <Redirect to="/Home" />;
  }

  return (
    <div>
      <div className="subpage">
        <div className="subpage-image forget-image">
          <HeaderWhite />
        </div>
        <div className="subpage-right">
          <div className="form">
            <h3>Remember me my password</h3>
            <form onSubmit={handleForget}>
              <label>
                <input name="email" type="email" placeholder="Email" />
              </label>
              <div className="text-center">
                <button className="button" type="submit">Send</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(ForgetPassword);
