import React, { useCallback, useContext } from "react";
import { withRouter, Redirect } from "react-router";
import firebaseConfig from "../firebase";
import { AuthContext } from "../Auth.js";
import HeaderWhite from "./HeaderWhite";
import { Link } from "react-router-dom";

const SignUp = ({ history }) => {
  const handleSignUp = useCallback(
    async (event) => {
      event.preventDefault();
      const { email, password } = event.target.elements;
      try {
        await firebaseConfig
          .auth()
          .createUserWithEmailAndPassword(email.value, password.value);
        history.push("/Home");
      } catch (error) {
        alert(error);
      }
    },
    [history]
  );

  const { currentUser } = useContext(AuthContext);

  if (currentUser) {
    return <Redirect to="/Home" />;
  }

  return (
    <div>
      <div className="subpage">
        <div className="subpage-image sign-in-image">
          <HeaderWhite />
        </div>
        <div className="subpage-right">
          <div className="form">
            <h3>Sign Up</h3>
            <form onSubmit={handleSignUp}>
              <label>
                <input type="text" name="name" id="name" placeholder="Name" />
              </label>
              <label>
                <input type="text" name="username" id="username" placeholder="Username" />
              </label>
              <label>
                <input name="email" type="email" placeholder="Email" />
              </label>
              <label>
                <input name="password" type="password" placeholder="Password" />
              </label>
              <div className="text-center">
                <button className="button" type="submit">Sign Up</button>
              </div>
            </form>
            <div className="text-center">
              <Link to="/Login">I have already an account!</Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(SignUp);
