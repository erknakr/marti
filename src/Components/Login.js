import React, { useCallback, useContext } from "react";
import { withRouter, Redirect } from "react-router";
import firebaseConfig from "../firebase.js";
import { AuthContext } from "../Auth.js";
import HeaderWhite from "./HeaderWhite";
import { Link } from "react-router-dom";

const Login = ({ history }) => {
  const handleLogin = useCallback(
    async (event) => {
      event.preventDefault();
      const { email, password } = event.target.elements;
      try {
        await firebaseConfig
          .auth()
          .signInWithEmailAndPassword(email.value, password.value);
        history.push("/Home");
      } catch (error) { 
        alert(error);
      }
    },
    [history]
  );

  const { currentUser } = useContext(AuthContext);

  if (currentUser) {
    return <Redirect to="/Home" />;
  }

  return (
    <div>
      <div className="subpage">
        <div className="subpage-image log-in-image">
          <HeaderWhite />
        </div>
        <div className="subpage-right">
          <div className="form">
            <h3>Login</h3>
            <form onSubmit={handleLogin}>
              <label>
                <input name="email" type="email" placeholder="Email" />
              </label>
              <label>
                <input name="password" type="password" placeholder="Password" />
              </label>
              <div>
                <Link to="/forget-password">
                I forget my password!
                </Link>
              </div>
              <div className="text-center">
                <button className="button" type="submit">Log in</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(Login);
