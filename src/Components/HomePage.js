import React from "react";
import Header from './Header';
import { Link } from "react-router-dom";

const HomePage = () => {
    return (
        <div>
            <Header />
            <section className="main">
                <div className="container-fluid">
                    <div className="row align-items-center">
                        <div className="col-12">
                            <h1>A new experience!</h1>
                            <p>A great chance for a new To-Do experience.</p>
                            <Link to="/SignUp" className="button">Sign In</Link>
                            <Link to="/Login" className="button">Log In</Link>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
};

export default HomePage;