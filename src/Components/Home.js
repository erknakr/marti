import React, { Component, useContext } from "react";
import { Redirect } from "react-router";
import firebase from "firebase";
import firebaseConfig from "../firebase";
import logo from "../assets/img/Logo.png";
import AddTask from "./AddTask";
import Task from "./Task";
import { AuthContext } from "../Auth.js";

class Home extends Component {

  
  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
    };
  }

  getData = () => {

    const user = firebase.auth().currentUser;
    const email = user.uid;
  
    return firebase
      .database()
      .ref("/to-do/" + email)
      .once("value")
      .then((dataSnapshot) => {
        const state = dataSnapshot.val();
        this.setState({
          tasks: state,
        });
      });
  };

  componentDidMount() {
    this.getData();
  }

  refresh = () => {
    this.getData();
  }

  render() {
    let tasks = this.state.tasks;
    
    return (
      <div>
        <header>
          <div className="container-fluid">
            <img src={logo} className="App-logo" alt="logo" />
          </div>
        </header>

        <div className="home">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <AddTask refresh={this.refresh} />
              </div>
              <div className="col-12"
                    onClick={this.refresh}>
                {Object.keys(tasks).map((task, index) => (
                  <Task
                    date={task}
                    task={tasks[task].task}
                    active={tasks[task].active}
                  />
                ))}
              </div>
            </div>
          </div>

          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <button
                  className="button"
                  onClick={() => firebaseConfig.auth().signOut()}
                >
                  Sign Out
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
