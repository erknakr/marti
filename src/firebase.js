import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = firebase.initializeApp ({
    apiKey: "AIzaSyBQy4usaDjnciFoocqE68IT0UZ48dZZE-U",
    authDomain: "marti-42d80.firebaseapp.com",
    databaseURL: "https://marti-42d80.firebaseio.com",
    projectId: "marti-42d80",
    storageBucket: "marti-42d80.appspot.com",
    messagingSenderId: "355502015877",
    appId: "1:355502015877:web:cddb609d6d6c961cb670ec"
});

export default firebaseConfig;